#include "mesh.h"

Mesh::Mesh()
{
    initializeOpenGLFunctions();
    indexCount = 0;
}

Mesh::~Mesh()
{
    vbo.destroy();
    ebo.destroy();
    vao.destroy();
}

void Mesh::initialize()
{
    vao.create();
    vao.bind();

    createCube();

    vbo.create();
    vbo.bind();
    vbo.allocate(vertices, sizeof(vertices));

    ebo.create();
    ebo.bind();
    ebo.allocate(indices, sizeof(indices));

    glEnableVertexAttribArray(0); // L'indice 0 correspond aux coordonnées des sommets
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

    vao.release();
}

void Mesh::draw(QOpenGLShaderProgram *shaderProgram)
{
    vao.bind();
    glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
    vao.release();
}

void Mesh::createCube()
{
    GLfloat vertices[] = {
        // Face avant
        -0.5f, -0.5f,  0.5f,
         0.5f, -0.5f,  0.5f,
         0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f,  0.5f,
        // Face arrière
        -0.5f, -0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f,  0.5f, -0.5f,
        -0.5f,  0.5f, -0.5f
    };

    GLuint indices[] = {
        0, 1, 2,
        2, 3, 0,
        4, 5, 6,
        6, 7, 4,
        0, 4, 7,
        7, 3, 0,
        1, 5, 6,
        6, 2, 1,
        0, 1, 5,
        5, 4, 0,
        2, 3, 7,
        7, 6, 2
    };

    indexCount = sizeof(indices) / sizeof(indices[0]);

    memcpy(vertices, vertices, sizeof(vertices));
    memcpy(indices, indices, sizeof(indices));
}
