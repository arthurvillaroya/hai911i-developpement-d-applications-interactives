#ifndef MESH_H
#define MESH_H

#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>

class Mesh : protected QOpenGLFunctions
{
public:
    Mesh();
    ~Mesh();

    void initialize();
    void draw(QOpenGLShaderProgram *shaderProgram);

private:
    void createCube();

    QOpenGLVertexArrayObject vao;
    QOpenGLBuffer vbo;
    QOpenGLBuffer ebo;
    GLfloat *vertices;
    GLuint *indices;
    int indexCount;
};

#endif // MESH_H
